@echo off
setlocal

set /p "target_dir=Enter the directory path: "
if not exist "%target_dir%" (
    echo Error: Directory "%target_dir%" does not exist.
    exit /b 2
)

:choose_attribute
echo Choose an attribute to set for files in "%target_dir%":
echo 1. Hidden
echo 2. Readonly
echo 3. Archive
set /p "attr_choice=Enter your choice (1-3): "

if "%attr_choice%"=="1" (
    set "attr=hidden"
) else if "%attr_choice%"=="2" (
    set "attr=readonly"
) else if "%attr_choice%"=="3" (
    set "attr=archive"
) else (
    echo Error: Invalid choice.
    goto choose_attribute
)

echo You chose to set the attribute %attr% for files in "%target_dir%".
set /p "confirm=Do you want to proceed? (y/n): "
if /i "%confirm%" neq "y" (
    echo Operation cancelled.
    exit /b 0
)
if "%attr%"=="hidden" (
    attrib +h "%target_dir%\*.*"
) else if "%attr%"=="readonly" (
    attrib +r "%target_dir%\*.*"
) else if "%attr%"=="archive" (
    attrib +a "%target_dir%\*.*"
) else (
    echo Error: Invalid attribute specified.
    exit /b 3
)

echo Operation completed successfully.
pause
exit /b 0